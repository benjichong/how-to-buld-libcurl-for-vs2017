# How to buld libcurl for VS2017

## Supported Visual Studio are:
*  2017

## Steps
1. Start VS2017 Console: `"[Program Files (x86)]\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64`
1. Download and unzip curl-[latest].zip at https://curl.haxx.se/download.html
1. Navigate to "curl-[latest]\winbuild" (optionally mode can be set as ‘dll’ to have later one more DLL do deal with in the project)
1. Execute "set RTLIBCFG=static" for static compile (\MT instead of \MD)
1. Execute "nmake /f Makefile.vc mode=static VC=14 ENABLE_IPV6=no MACHINE=AMD64"
1. Grab results in "/builds/libcurl-vc14-AMD64-release-static-sspi-winssl"
1. Setup new project’s ‘include’ and ‘lib’ folder (put libcurl_a.lib or libcurl.lib into references)
1. Look at samples: https://curl.haxx.se/libcurl/c/example.html